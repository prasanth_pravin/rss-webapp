export default {
  AuthapiBasepath: 'http://stagingauthapi.readysetsurgical.com/api/',
  WebapiBasepath: 'http://stagingwebapi.readysetsurgical.com/api/'
}