import { createBrowserApp } from '@react-navigation/web';
import { createStackNavigator } from 'react-navigation';

//import MainTabNavigator from './MainTabNavigator';
import LoginScreen from '../screens/LoginScreen';
import HomeScreen from '../screens/HomeScreen';
import CaseScreen from '../screens/CaseScreen';

const stackNavigator = createStackNavigator({
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
  //Main: MainTabNavigator,
  LoginScreen: LoginScreen,
  HomeScreen: HomeScreen,
  CaseScreen: CaseScreen,
});
stackNavigator.path = '';

export default createBrowserApp(stackNavigator, { history: 'hash' });
