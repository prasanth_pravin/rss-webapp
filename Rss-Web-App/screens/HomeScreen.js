import * as WebBrowser from 'expo-web-browser';
import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  Platform,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Dimensions
} from 'react-native';
import ProductTile from './ProductTile';
import CaseScreen from './CaseScreen';

const ITEM_WIDTH = Dimensions.get('window').width

export default class HomeScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: {
        backgroundColor: navigation.getParam('BackgroundColor', '#8CC64A'),
      },
      headerTintColor: navigation.getParam('HeaderTintColor', '#fff'),
      title: 'Home',
    };
  };

  // static navigationOptions = ({ navigation }) => {
  //   return {
  //     //Heading/title of the header
  //     title: navigation.getParam('Title', 'Left Right Custom Header'),
  //     //Heading style
  //     headerStyle: {
  //       backgroundColor: navigation.getParam('BackgroundColor', '#8CC64A'),
  //     },
  //     //Heading text color
  //     headerTintColor: navigation.getParam('HeaderTintColor', '#fff'),

  //     headerRight: (
  //       <TouchableOpacity >
  //         <Text
  //           style={{
  //             color: 'white',
  //           }}>
  //           Right Menu
  //         </Text>
  //       </TouchableOpacity>
  //     ),
  //     headerLeft: (
  //       <View>
  //         <TouchableOpacity >
  //           <Image source={require('../assets/images/home.png')} style={{ height: '75%', width: '100%', resizeMode: 'contain' }} />
  //         </TouchableOpacity>
  //       </View>
  //     ),
  //   };
  // };

  constructor(props) {
    super(props);
    this.state = {
      refreshing: true,
      result: []
    };

    this.fetchProducts = this.fetchProducts.bind(this);
  }

  componentDidMount() {
    this.handleRefresh();
  }


  handleRefresh() {
    this.setState(
      { refreshing: true },
      () => this.fetchProducts()
    )
  }

  fetchProducts() {
    this.getProducts()
      .then(result => this.setState({ result, refreshing: false }, () => {
      }))
      .catch(() => this.setState({ refreshing: false }));
  }

  async getProducts() {
    let result = [{ "name": "My Cases", "label": "বাংলা", "code": "bn" }
      , { "name": "Scheduling Feed", "label": "English", "code": "en" }
      , { "name": "Inventory Libraries", "label": "ગુજરાતી", "code": "gu" }];

    return result;
  }

  HandleNavigation(index) {
    if (index === 0) {
      this.props.navigation.navigate('CaseScreen');
    }
  }

  render() {
    return (
      <View style={styles.MainView}>
        {Platform.OS === 'web' ?
          <FlatList
            data={this.state.result}
            numColumns={3}
            renderItem={({ item, index }) => {
              return (
                <View>
                  <TouchableOpacity onPress={() => this.HandleNavigation(index)}>
                    <ProductTile itemWidth={(ITEM_WIDTH - 30) / 3} result={item} />
                  </TouchableOpacity>
                </View>
              )
            }}
            refreshing={this.state.refreshing}
          />
          :
          <FlatList
            data={this.state.result}
            numColumns={2}
            renderItem={({ item, index }) => {
              return (
                <View>
                  <TouchableOpacity onPress={() => this.HandleNavigation(index)}>
                    <ProductTile itemWidth={(ITEM_WIDTH - 20) / 2} result={item} />
                  </TouchableOpacity>
                </View>
              )
            }}
            refreshing={this.state.refreshing}
          />}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainView: {
    flex: 1,
    marginTop: '1%'
  },
  searchViewStyles: {
    height: '100%'
  },
  TitleStyles:
  {
    justifyContent: 'center',
    width: '80%'
  },
  SubView: {
    alignSelf: 'baseline',
    flexDirection: "row",
    justifyContent: 'center',
    marginBottom: '2%',
  },
  headerTitleStyles: {
    fontSize: 18,
    fontWeight: '900',
    textAlign: 'center',
    marginRight: '15%',
    marginLeft: '4%',
    marginTop: '12%',
    marginBottom: '5%'
  },
  spinnerTextStyle: {
    color: '#FFFFFF',
  },
  imageStyles: {
    height: '15%',
    width: '100%',
    resizeMode: 'contain'
  },
});

