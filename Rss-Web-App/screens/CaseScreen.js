import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator
} from 'react-native';
import CaseList from "./CaseList";

export default class CaseScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: {
        backgroundColor: navigation.getParam('BackgroundColor', '#8CC64A'),
      },
      headerTintColor: navigation.getParam('HeaderTintColor', '#fff'),
      title: 'Cases',
    };
  };

  state = {
    cases: [],
    loading: true
  };
  componentDidMount() {
    this.getCases();
  }

  async getCases() {
    const res = await fetch('https://stagingwebapi.readysetsurgical.com/api/User/Get1InvoiceCasesByUserId?p_UserId=378', {
      method: 'GET',
      headers: {
        'Authorization': 'Basic YWRtaW46UmVhZHlTZXRBUElfMjAxOA==',
      },
    }).then((response) => response.json())
      .then((responseJson) => {
        //alert(JSON.stringify(responseJson[0].RSID + ' ' + responseJson[0].CaseName));
        this.setState({ cases: [...responseJson], loading: false });
      }).catch((error) => {
        console.error(error);
      });

  }

  render() {
    return (
      <ScrollView noSpacer={true} noScroll={true} style={styles.container}>
        {this.state.loading ? (
          <ActivityIndicator
            style={[styles.centering]}
            color="#ff8179"
            size="large"
          />
        ) : (
            <CaseList cases={this.state.cases} />
          )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "whitesmoke"
  },
  centering: {
    alignItems: "center",
    justifyContent: "center",
    padding: 8,
  },
});

