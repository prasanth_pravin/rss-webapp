import * as WebBrowser from 'expo-web-browser';
import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    Image,

} from 'react-native';
import { Card } from 'react-native-cards';

export default class ProductTile extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const {
            name,
            label,
            code
        } = this.props.result;
        const { itemWidth } = this.props;
        return (
            <View style={{ height: 130, width: itemWidth, margin: 5 }}>
                <Card style={styles.cardStyle}>
                    <View >
                        <Text style={styles.tileTextStyles} numberOfLines={5}>
                            {name}
                        </Text>
                    </View>
                </Card>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    CategoryStyle: {
        alignSelf: 'baseline',
    },
    tileTextStyles: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#ffffff',
    },
    cardStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#8B8B8B',
    }
});