import React from "react";
import { SwipeableFlatList } from "react-native";
import CaseItem from "./CaseItem";
import Action from "./Action";

const CaseList = ({ cases }) => {
  return (
    <SwipeableFlatList
      data={cases}
      bounceFirstRowOnMount={true}
      maxSwipeDistance={160}
      renderItem={CaseItem}
      renderQuickActions={Action}
    />
  );
};

export default CaseList;