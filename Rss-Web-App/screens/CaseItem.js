import React from "react";
import { View, Image, Text, StyleSheet } from "react-native";

const CaseItem = ({ item: user }) => {
  return (
    <View style={styles.row}>
      {/* <Image style={styles.rowIcon} source={{ uri: user.picture.medium }} /> */}
      <View style={styles.rowData}>
        <Text style={styles.rowDataText}>{`${'Case Name: ' + user.CaseName} ${
          '\nSurgeon Name: ' + user.Surgeon
          } ${'\nVendor Company: ' + user.VendorCompany}`}</Text>
        <Text style={styles.rowDataSubText}>{'RSID: ' + user.RSID}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    padding: 15,
    marginBottom: 5,
    backgroundColor: "white",
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  rowIcon: {
    width: 50,
    height: 50,
    marginRight: 20,
    borderRadius: 25,
    //boxShadow: "0 1px 2px 0 rgba(0,0,0,0.1)"
  },
  rowData: {
    flex: 1
  },
  rowDataText: {
    fontSize: 15,
    textTransform: "capitalize",
    color: "#4b4b4b"
  },
  rowDataSubText: {
    fontSize: 13,
    opacity: 0.8,
    color: "#a8a689",
    marginTop: 4
  }
});

export default CaseItem;