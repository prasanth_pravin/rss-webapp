import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Keyboard,
  TextInput,
  Image,
  ScrollView,
  Platform,
  Dimensions
} from 'react-native';
import { Button } from 'react-native-elements';
import HomeScreen from './HomeScreen';
import SecureFetch from '../SecureFetch';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

let styl = '';
var { height, width } = Dimensions.get('window');

export default class App extends React.Component {
  static navigationOptions = { header: null }

  LoginNavigation() {
    this.props.navigation.navigate('HomeScreen');
  }
  render() {
    if (width > 1024 && height > 768) {
      styl = styles.textWrapperWeb;
    } else {
      styl = styles.textWrapperApp;
    }
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <View style={styles.container}>
          <View style={styl}>
            <ScrollView>

              <Image source={require('../assets/images/logo.png')} style={styles.imageStyles} />
              <TextInput
                placeholder="User Name"
                style={styles.inputTextStyles}
              />
              <TextInput
                placeholder="Password"
                style={styles.inputTextStyles}
                secureTextEntry={true}
              />
              <Button
                buttonStyle={styles.loginButton}
                onPress={() => this.LoginNavigation()}
                title="Login"
              />
              <View style={styles.dottedStyles} />
              <Text style={styles.newUserStyles}> New User? </Text>
              <Button
                buttonStyle={styles.registerButton}
                // onPress={() => this.onLoginPress()}
                title="Register Now"
              />
              <Button
                buttonStyle={styles.registerButton}
                // onPress={() => this.onLoginPress()}
                title="Forgot Password"
              />

            </ScrollView>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#9D9D9D',
    alignItems: 'center'
  },
  textWrapperApp: {
    height: '100%',
    width: wp('100%'),
  },
  textWrapperWeb: {
    height: hp('70%'),
    width: wp('30%'),
  },
  myText: {
    fontSize: hp('5%')
  },
  inputTextStyles: {
    marginLeft: '4%',
    marginRight: '4%',
    height: 43,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    padding: 10,
    margin: '2%',
    fontSize: 14
  },
  imageStyles: {
    marginTop:'10%',
    height: 150,
    width: '100%',
    resizeMode: 'contain'
  },
  loginButton: {
    backgroundColor: '#8CC64A',
    borderRadius: 5,
    height: 45,
    marginTop: 10,
    marginLeft: '4%',
    marginRight: '4%',
    fontWeight: 'bold',
  },
  registerButton: {
    backgroundColor: '#8B8B8B',
    borderRadius: 5,
    height: 45,
    marginTop: 10,
    marginLeft: '4%',
    marginRight: '4%',
  },
  dottedStyles: {
    marginLeft:'4%',
    marginRight:'4%',
    marginTop:'4%',
    borderStyle: 'dotted',
    borderWidth: 1,
    borderRadius: 5,
    margin: '2%',
    color: '#FFFFFF'
  },
  newUserStyles: {
    textAlign: 'center',
    color: '#FFFFFF',
    fontSize: 18
  }
});